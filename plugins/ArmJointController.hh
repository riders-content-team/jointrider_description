#ifndef ARM_JOINT_CONTROLLER_HH
#define ARM_JOINT_CONTROLLER_HH

#include "gazebo/common/common.hh"
#include "ros/ros.h"
#include "gazebo/physics/PhysicsTypes.hh"
#include "gazebo/physics/MultiRayShape.hh"
#include "gazebo/physics/World.hh"
#include "gazebo/physics/Model.hh"
#include "gazebo/physics/Joint.hh"
#include "gazebo/physics/PhysicsIface.hh"
#include "std_srvs/Trigger.h"

#include "std_msgs/Float64.h"

namespace gazebo
{
    class GAZEBO_VISIBLE ArmJointController : public ModelPlugin
    {
    public:
        ArmJointController();

    public:
        virtual ~ArmJointController();

    private:
        virtual void Load(physics::ModelPtr _model, sdf::ElementPtr _sdf);
        virtual void PosCallback(const std_msgs::Float64::ConstPtr& msg);
        virtual void Update();
        virtual bool IsPositonSet(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res);

        event::ConnectionPtr world_update;

        physics::WorldPtr world;
        physics::ModelPtr model;
        physics::JointPtr joint;

        std::unique_ptr<ros::NodeHandle> rosNode;
        ros::Subscriber position_subscriber;
        ros::ServiceServer joint_service;

        std::string robot_name, joint_name;
        double update_period, last_update_time;
        double  joint_position, pose_cmd;
        double lower_limit, upper_limit;

        bool position_arrived;
    };
}

#endif