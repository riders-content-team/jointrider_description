#include "ArmJointController.hh"

using namespace gazebo;

GZ_REGISTER_MODEL_PLUGIN(ArmJointController)

ArmJointController::ArmJointController()
{
}

ArmJointController::~ArmJointController()
{
}

void ArmJointController::Load(physics::ModelPtr _model, sdf::ElementPtr _sdf)
{
    this->model = _model;
    this->world = this->model->GetWorld();

    this->robot_name = this->model->GetName();

    if (_sdf->HasElement("joint_name"))
    {
        this->joint_name = _sdf->Get<std::string>("joint_name");
    }

    double update_rate = 50;
    if (_sdf->HasElement("updateRate"))
    {
        update_rate = _sdf->Get<double>("updateRate");
    }

    this->update_period = 1.0 / update_rate;

    this->last_update_time = this->world->SimTime().Double();

    this->joint = this->model->GetJoint(this->joint_name);
    this->joint_position = this->joint->Position(0);
    this->pose_cmd = 0;

    this->lower_limit = this->joint->LowerLimit(0);
    this->upper_limit = this->joint->UpperLimit(0);

    // Initialize ros, if it has not already bee initialized.
    if (!ros::isInitialized())
    {
        int argc = 0;
        char **argv = NULL;
        ros::init(argc, argv, this->robot_name,
                  ros::init_options::NoSigintHandler);
    }

    // Create our ROS node. This acts in a similar manner to
    // the Gazebo node
    this->rosNode.reset(new ros::NodeHandle(this->robot_name));

    std::string command_topic = "/" + this->robot_name + "/" + this->joint_name + "/set_pose";

    ros::SubscribeOptions so =
        ros::SubscribeOptions::create<std_msgs::Float64>(
            command_topic,
            1,
            std::bind(&ArmJointController::PosCallback, this, std::placeholders::_1),
            ros::VoidConstPtr(),
            NULL);

    this->position_subscriber = this->rosNode->subscribe(so);

    this->joint_service = this->rosNode->advertiseService<std_srvs::Trigger::Request, std_srvs::Trigger::Response>
        ("/" + this->robot_name + "/" + this->joint_name + "/is_set", std::bind(&ArmJointController::IsPositonSet, this, std::placeholders::_1, std::placeholders::_2));

    this->world_update = event::Events::ConnectWorldUpdateBegin(
        std::bind(&ArmJointController::Update, this));

}

void ArmJointController::Update()
{
    double current_time = this->world->SimTime().Double();
    this->joint_position = this->joint->Position(0);

    if (current_time - this->last_update_time >= this->update_period)
    {
        if (this->pose_cmd - this->joint_position >= 0.02)
        {
            this->joint_position += 0.02;
            this->position_arrived = false;
        }
        else if (this->pose_cmd - this->joint_position < -0.02)
        {
            this->joint_position -= 0.02;
            this->position_arrived = false;
        }
        else 
        {
            this->joint_position = this->pose_cmd;
            this->position_arrived = true;
        }

        this->joint->SetPosition(0, this->joint_position);
        this->last_update_time = current_time;
    }
}

void ArmJointController::PosCallback(const std_msgs::Float64::ConstPtr &msg)
{
    double data = msg->data;
    if (data >= this->upper_limit)
    {
        data = this->upper_limit;
    }
    else if (data <= this->lower_limit)
    {
        data = this->lower_limit;
    }

    this->pose_cmd = data;
}

bool ArmJointController::IsPositonSet(std_srvs::Trigger::Request &req, std_srvs::Trigger::Response &res)
{
    res.success = this->position_arrived;

    return true;
}
